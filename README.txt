
Package: PCLOG (Pro-Contra-Log)

Author: Kili�n Imre (kilian@gamma.ttk.pte.hu)

Version: 1.0

Download: ????????

PCLOG:

An unified Prolog-Contralog programming environment.
Contralog is a Prolog-conform language that, instead of
backward-chaining, uses rather forward chaining strategy.
Contralog clauses are thus very similar to those of Prolog.

Contralog clauses can be mixed with Prolog clauses, to switch
to Contralog and back to Prolog is possible with help of
declarations.

Example of use:

Installation:

:-pack_install('...full path of pclog-1.0.zip').

Pythagoras' triads... for the first success will generate the first one,
and for pressing ";" you will get next and next triads...

:-[pack(pclog/tests/test_p3)].
%%starts the program
:-p3:goal,fail.
%%cleans blackboard
:-p3:clean,fail.

Matrix chain-multiplication problem...

:-[pack(pclog/tests/test_mx)].
%%starts the program, and cleans blackboard beforehead
:-start.


Floyd-Warshall algorithm...

:-[pack(pclog/tests/test_wsh)].
%%starts the program and cleans blackboard beforehead
:-start.



Debugging: For efficient use of the package at the moment, the Prolog debugger
can be used. It is somehow a low-level tool, to use it, it is necessary
to recognize Contralog in its compiled, Prolog form. To overcome this
an initial version of a Contralog debugger is provided in file
pack(pclog/prolog/ctrace)... It is basically a very simple interpreted
debugger without any breakpoint or any interactive possibility...

Using the debugger (if once it has been loaded)
:-trstart(GOAL).

The debugger uses a 6 port executional model with the following ports:
- >	entering
- +	success
- -	fail
- <	backtrace
- #	asserting a new clause 
- @	export clause (needs an imported Prolog predicate)



Contents of the package

README.txt			this file
LICENSE				Conditions for licensing (LGPL)
ApplicationOfContralog.PDF	Article about Contralog
Twoway.doc			Article about Contralog
SICSTUSProContraLog.doc		Article about SICSTus PC-LOG (outdated)
SWIProContraLog.doc		Article about SWI PC-LOG
pack.pl				Package descriptor

prolog/clog.pl			Contralog macro extension module
prolog/ctrace.pl		Contralog debugger facility

tests/p3.pl			Pythagoras' triads
tests/test_p3.pl		Pythagoras' triads testbed
tests/mx.pl			Matrix-chain-optimisation in Contralog
tests/test_mx.pl		Matrix-chain-optimisation testbed
tests/warshall.pl		Floyd-Warshall algorithm in Contralog
tests/test_wsh.pl		Floyd-Warshall algorithm testbed

