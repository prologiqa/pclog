% Changing the depth-first strategy to breadth-first
% Cock-Younger-Casami algorithm with Contralog
% Kili�n, Imre 17-dec-2016.
%
% a Contra-DCG kl�zok k�z� kellene Contralog h�v�sokat tenni tudni...
% (jelenleg a {} sima Prolog h�v�sokk� fordul...)
% az egyik k�rd�s: a level=level+1 tudja-e el�bb triggerelni az �sszes
% egy�b Contralog kl�zt, �s csak ut�na n�velni a szintsz�mot?
% ha nem, akkor k�v�lr�l kell a szintsz�mot n�velni, �s a triggerel�
% h�v�st k�zzel megh�vni...
% M�g egy lehet�s�g a DCG-Contralog k�zi ford�t�s 





:- file_search_path(app_preferences,PREF), atom(PREF),
	atom_concat(PREF,'/Documents/PROLOG',DIR),
	working_directory(_,DIR),
	true.

:- use_module(pack('pclog/prolog/clog')).

:- discontiguous clean/0.

user:exp_szam(N,M,0,Y):- nb_getval(ennyi,Y),
	write(N-M-0-Y),nl.

start(L):-
  clean, fail;
  length(L,Y), nb_setval(ennyi,Y),
  nth0(NTH,L,X), NEXT is NTH+1,fire_token(X,NTH,NEXT),fail;
  goal, fail.

:- contra.

:- export([szam/4]).
:- import([token/3]).
:- import([next/0]).



level(0).

%:- egykilenc(X,['nyolc'],[]).

kettokilenc(2,1)-->token('kett�').
kettokilenc(3,1)-->token('h�rom').
kettokilenc(4,1)-->token('n�gy').
kettokilenc(5,1)-->token('�t').
kettokilenc(6,1)-->token('hat').
kettokilenc(7,1)-->token('h�t').
kettokilenc(8,1)-->token('nyolc').
kettokilenc(9,1)-->token('kilenc').

egykilenc(1,1)-->token('egy').
egykilenc(ENNYI,NEXT,X0,X):-kettokilenc(ENNYI,PREV,X0,X), level(PREV), {NEXT is PREV+1}.

harminckilencven(30,1)-->token('harminc').
harminckilencven(40,1)-->token('negyven').
harminckilencven(50,1)-->token('�tven').
harminckilencven(60,1)-->token('hatvan').
harminckilencven(70,1)-->token('hetven').
harminckilencven(80,1)-->token('nyolcvan').
harminckilencven(90,1)-->token('kilencven').

tizkilencven(10,1)-->token('t�z').
tizkilencven(20,1)-->token('h�sz').
tizkilencven(ENNYI,NEXT,X0,X):-harminckilencven(ENNYI,PREV,X0,X), level(PREV), {NEXT is PREV+1}.

tizenkilencven(10,1)-->token('tizen').
tizenkilencven(20,1)-->token('huszon').
tizenkilencven(ENNYI,NEXT,X0,X):-tizkilencven(ENNYI,PREV,X0,X), level(PREV), {NEXT is PREV+1}.

egykkilenc(ENNYI,NEXT,X0,X):-egykilenc(ENNYI,PREV,X0,X), level(PREV), {NEXT is PREV+1}.
egykkilenc(ENNYI,NEXT,X0,X):-tizkilencven(ENNYI,PREV,X0,X), level(PREV), {NEXT is PREV+1}.
egykkilenc(ENNYI,NEXT,X0,X):-tizenkilencven(MENNYI,PREV1,X0,X1), level(PREV1),
                         egykilenc(ANNYI,PREV2,X1,X),
						 {NEXT is max(PREV1,PREV2)+1}, {ENNYI is MENNYI+ANNYI}.

szaz(100,1)-->token('sz�z').

nehanyszaz(ENNYI,NEXT,X0,X):-szaz(ENNYI,PREV,X0,X), level(PREV), {NEXT is PREV+1}.
nehanyszaz(ENNYI,NEXT,X0,X):-kettokilenc(MENNYI,PREV1,X0,X1), level(PREV1),
                    szaz(ANNYI,PREV2,X1,X),
					{NEXT is max(PREV1,PREV2)+1},
					{ENNYI is ANNYI*MENNYI}.

egykkkilenc(ENNYI,NEXT,X0,X):-egykkilenc(ENNYI,PREV,X0,X), level(PREV), {NEXT is PREV+1}.
egykkkilenc(ENNYI,NEXT,X0,X):-nehanyszaz(ENNYI,PREV,X0,X), level(PREV), {NEXT is PREV+1}.
egykkkilenc(ENNYI,NEXT,X0,X):-nehanyszaz(ANNYI,PREV1,X0,X1), egykkilenc(MENNYI,PREV2,X1,X), level(PREV2),
                    {NEXT is max(PREV1,PREV2)+1},
                    {ENNYI is ANNYI+MENNYI}.

ezer(1000,1)-->token('ezer').

nehanyezer(ENNYI,NEXT,X0,X):-ezer(ENNYI,PREV,X0,X), level(PREV), {NEXT is PREV+1}.
nehanyezer(ENNYI,NEXT,X0,X):-egykkkilenc(ANNYI,PREV1,X0,X1), level(PREV1), ezer(MENNYI,PREV2,X1,X),
                    {NEXT is max(PREV1,PREV2)+1}, 
                    {ENNYI is ANNYI*MENNYI}.

egykkkkilenc(ENNYI,NEXT,X0,X):-egykkkilenc(ENNYI,PREV,X0,X), level(PREV), {NEXT is PREV+1}.
egykkkkilenc(ENNYI,NEXT,X0,X):-nehanyezer(ENNYI,PREV,X0,X), level(PREV), {NEXT is PREV+1}.
egykkkkilenc(ENNYI,NEXT,X0,X):-nehanyezer(ANNYI,PREV1,X0,X1), egykkkilenc(MENNYI,PREV2,X1,X), level(PREV2),
                         {NEXT is max(PREV1,PREV2)+1},
                         {ENNYI is ANNYI+MENNYI}.

szam(0,1)-->token('nulla').
szam(ENNYI,NEXT,X0,X):-egykkkkilenc(ENNYI,PREV,X0,X), level(PREV), {NEXT is PREV+1}.


level(NEXT):- level(PREV), {PREV<30, NEXT is PREV+1}.


:-pro.











