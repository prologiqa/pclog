:- file_search_path(app_preferences,PREF), atom(PREF),
	atom_concat(PREF,'/Documents/PROLOG',DIR),
	working_directory(_,DIR),
	true.

user:file_search_path(pack,app_preferences('PROLOG/dir')).




:- use_module('pclog/prolog/clog').

user:exp_szam(N,0,Y):- nb_getval(ennyi,Y),
	write(N-0-Y),nl.

start(L):- clean, fail;
  length(L,Y), nb_setval(ennyi,Y),
  nth0(NTH,L,X), NEXT is NTH+1,fire_token(X,NTH,NEXT).

:- contra.

:- export([szam/3]).
:- import([token/3]).

%:- egykilenc(X,['nyolc'],[]).

kettokilenc(2)-->token('kett�').
kettokilenc(3)-->token('h�rom').
kettokilenc(4)-->token('n�gy').
kettokilenc(5)-->token('�t').
kettokilenc(6)-->token('hat').
kettokilenc(7)-->token('h�t').
kettokilenc(8)-->token('nyolc').
kettokilenc(9)-->token('kilenc').

egykilenc(1)-->token('egy').
egykilenc(ENNYI)-->kettokilenc(ENNYI).

harminckilencven(30)--> token('harminc').
harminckilencven(40)-->token('negyven').
harminckilencven(50)-->token('�tven').
harminckilencven(60)-->token('hatvan').
harminckilencven(70)-->token('hetven').
harminckilencven(80)-->token('nyolcvan').
harminckilencven(90)-->token('kilencven').

tizkilencven(10)-->token('t�z').
tizkilencven(20)-->token('h�sz').
tizkilencven(ENNYI)-->harminckilencven(ENNYI).

tizenkilencven(10)-->token('tizen').
tizenkilencven(20)-->token('huszon').
tizenkilencven(ENNYI)-->tizkilencven(ENNYI).

egykkilenc(ENNYI)-->egykilenc(ENNYI).
egykkilenc(ENNYI)-->tizkilencven(ENNYI).
egykkilenc(ENNYI)-->tizenkilencven(MENNYI),
                         egykilenc(ANNYI), {ENNYI is MENNYI+ANNYI}.

szaz(100)-->token('sz�z').

nehanyszaz(ENNYI)-->szaz(ENNYI).
nehanyszaz(ENNYI)-->kettokilenc(MENNYI),
                    szaz(ANNYI), {ENNYI is ANNYI*MENNYI}.

egykkkilenc(ENNYI)-->egykkilenc(ENNYI).
egykkkilenc(ENNYI)-->nehanyszaz(ENNYI).
egykkkilenc(ENNYI)-->nehanyszaz(ANNYI),egykkilenc(MENNYI),
                    {ENNYI is ANNYI+MENNYI}.

ezer(1000)-->token('ezer').

nehanyezer(ENNYI)-->ezer(ENNYI).
nehanyezer(ENNYI)-->egykkkilenc(ANNYI), ezer(MENNYI),
                    {ENNYI is ANNYI*MENNYI}.

egykkkkilenc(ENNYI)-->egykkkilenc(ENNYI).
egykkkkilenc(ENNYI)-->nehanyezer(ENNYI).
egykkkkilenc(ENNYI)-->nehanyezer(ANNYI), egykkkilenc(MENNYI),
                         {ENNYI is ANNYI+MENNYI}.

szam(0)-->token('nulla').
szam(ENNYI)-->egykkkkilenc(ENNYI).

:-pro.












