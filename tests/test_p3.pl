% Author: Kilián Imre
% Date: 2011.09.01.
%
% test pythagoras triads with PCLOG
% working directory should point to PCLOG home
%consult pclog
:- use_module(pack(pclog/prolog/clog)).

:- use_module(pack(pclog/tests/p3)).

exp_pyth3(X,Y,Z):-
  write([X-Y-Z]), nl.

:- export(exp_pyth3/3).

