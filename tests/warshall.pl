:- module(warshall,[]).

:- contra.

:- key(wm(+,+,+,-,-)).

:- import([w/1]).

:- wm(_,_,_,_,_).
wm(0,I,J,W,SIZE):- w(LL),
	{length(LL,SIZE), between(1,SIZE,I), nth1(I,LL,L),
	 between(1,SIZE,J), nth1(J,L,W)}.
wm(K,I,J,W,S):- wm(K1,I,J,K1IJ,S), wm(K1,I,K,K1IK,S), wm(K1,K,J,K1KJ,S),
	{K is K1+1, K1=<S},
	{W is min(K1IJ, K1IK+K1KJ)}.

:- pro.






